from flask import Flask, request, redirect, render_template
import requests
from flask_sqlalchemy import SQLAlchemy
import json
import datetime as time
import os

path = os.path.dirname(os.path.abspath(__file__))
db_file = "sqlite:///{}".format(os.path.join(path, "coinage.db"))

api = "https://free.currconv.com/api/v7/convert?apiKey=921abf5ad1363553a7d7"

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = db_file

db = SQLAlchemy(app)

class Record(db.Model):
    timestamp = db.Column(db.String(10), nullable=False, primary_key=True)
    fr = db.Column(db.String(10), nullable=False)
    to = db.Column(db.String(10), nullable=False)
    val = db.Column(db.String(10), nullable=False)
    

@app.route("/")
def list_db_values():
    records = Record.query.all()
    return render_template("home.html", records=records)

@app.route("/convert")
def convert_coinage():
    from_coin = request.args.get("fr")
    print(from_coin)
    to_coin   = request.args.get("to") 
    print(to_coin)
    currency = "%s_%s" % (from_coin,to_coin)
    print(currency)
    exchange  = get_exchange(currency)
    print(exchange["results"][currency]["val"])
    val = str(exchange["results"][currency]["val"])
    record = Record(timestamp = time.datetime.now(), fr = from_coin, to = to_coin, val = val)
    db.session.add(record)
    db.session.commit()
    return str(exchange["results"][currency]["val"])

    
def get_exchange(coinage):
    r = requests.get(api, params={"q" : coinage})
    return r.json()