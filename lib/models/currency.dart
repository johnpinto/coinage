class Currency{
  String flag;
  String code;
  String name;
  String country;

  Currency({this.flag, this.code, this.name, this.country});

  String get codeCool{
    return "$flag $code";
  }
}