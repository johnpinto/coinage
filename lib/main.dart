import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:Coinage/widgets/exchangeBar.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:Coinage/data/service.dart';
import 'package:Coinage/widgets/historyBar.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:decimal/decimal.dart';
import 'package:Coinage/models/currency.dart';

void main() => runApp(MyApp());


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Coinage',
      theme: ThemeData(
          primaryColor: Colors.blue, 
          accentColor: Colors.white),
      home: MyHomePage(title: 'Coinage'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  SharedPreferences prefs;
  List<String> history = [];
  var inputController = MoneyMaskedTextController(decimalSeparator: ",", thousandSeparator: ".");
  var outputController = MoneyMaskedTextController(decimalSeparator: ",", thousandSeparator: ".");

  Map<String, Currency> currencies = 
    { "BRL" : Currency(name: "Real Brasileiro", country: "Brasil", flag: "🇧🇷", code: "BRL"),
      "USD" : Currency(name: "Dólar Americano", country: "EUA",    flag: "🇺🇸", code: "USD"),
      "EUR" : Currency(name: "Euro",            country: "Europa", flag: "🇪🇺", code: "EUR"),
      "JPY" : Currency(name: "Yen Japonês",     country: "Japão",  flag: "🇯🇵", code: "JPY"),
      "CNY" : Currency(name: "Yuan Chinês",     country: "China",  flag: "🇨🇳", code: "CNY")};

  @override
  void initState() {
    super.initState();
    _loadPreferences();
    inputController.addListener(_inputListener);
  }

  @override
  void dispose(){
    inputController.dispose();
    outputController.dispose();
    super.dispose();
  }

  _inputListener() {
    outputController.updateValue(_convert(inputController.numberValue.toString()));
  }

  _convert(String value){
    return (Decimal.parse(value) * Decimal.parse(prefs.getString("conversion"))).toDouble();
  }

  _loadPreferences() async {
    prefs = await SharedPreferences.getInstance();
    print(prefs.getKeys());
    await prefs.setString("fr", "USD");
    await prefs.setString("to", "BRL");
    await prefs.setString("time", "");
    Service().getConversion();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title, style: TextStyle(fontFamily: "RobotoMono")),
        actions: <Widget>[
          //todo make action button for url input
          IconButton(
            icon: const Icon(Icons.settings),
            onPressed: () {},
          )
        ],
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Container(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0.0, 8.0, 0.0, 8.0),
                child: 
                Column(
                  children: <Widget>[
                    ExchangeBar(
                      inputController: inputController,
                      outputController: outputController,
                      currencies: currencies,
                      refresh: (){
                        setState(() {
                          outputController.updateValue(_convert(inputController.numberValue.toString()));
                        });
                      },
                      ),
                  ],
                ),
              )
              ),
              SizedBox(height: 10.0),
              HistoryBar(history),
              SizedBox(height: 10.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  numberButton("7"),
                  numberButton("8"),
                  numberButton("9"),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  numberButton("4"),
                  numberButton("5"),
                  numberButton("6"),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  numberButton("1"),
                  numberButton("2"),
                  numberButton("3"),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  saveButton(),
                  numberButton("0"),
                  delButton(),
                ],
              ),
          ],
        ),     
      ),
    );
  }
  Widget numberButton(String number){
    return MaterialButton(
      shape: CircleBorder(),
      elevation: 10.0,
      height: 100.0,
      child: Text(number,
      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30.0),),
      textColor: Theme.of(context).primaryColor,
      color: Theme.of(context).accentColor,
      onPressed: (){
        inputController.text = inputController.text + number;
      },
    );
  }

  Widget delButton(){
    return MaterialButton(
      shape: CircleBorder(),
      elevation: 10.0,
      height: 100.0,
      child: Icon(Icons.backspace, size: 30,),
      textColor: Theme.of(context).accentColor,
      color: Theme.of(context).primaryColor,
      onPressed: (){
        inputController.text = 
        (inputController.text.length > 0) ? 
          inputController.text.substring(0, inputController.text.length - 1) : 
          "";
      },
    );
  }

  Widget saveButton(){
    return MaterialButton(
      shape: CircleBorder(),
      elevation: 10.0,
      height: 100.0,
      child: Icon(Icons.save,size: 30,),
      textColor: Theme.of(context).accentColor,
      color: Theme.of(context).primaryColor,
      onPressed: () async {
        prefs = await SharedPreferences.getInstance();
        var input = currencies[prefs.getString("fr")];
        var output = currencies[prefs.getString("to")];
        var time = prefs.getString("time");
        print("${input.flag} ${input.code} ${inputController.text} ➡️ ${output.flag} ${output.code} ${outputController.text} $time");
        history.add("${input.flag} ${input.code} ${inputController.text} ➡️ ${output.flag} ${output.code} ${outputController.text} Data: $time");
        setState(() {});
      },
    );
  }
}


