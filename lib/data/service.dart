import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

class Service{
  final String url = "192.168.100.5:5000";
  final String path = "/convert";
  // base querry: http://127.0.0.1:5000/convert?fr=eur&to=BRL&amount=1

  void getConversion() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String frParam = prefs.getString("fr");
    print(frParam);
    String toParam = prefs.getString("to");
    print(toParam);
    var query = {"fr" : frParam, "to" : toParam};
    var response = await http.get(Uri.http(url, path, query));
    await prefs.setString("conversion", response.body);
    var format = DateFormat("dd/MM/yy");
    print(format.format(DateTime.now()));
    await prefs.setString("time", format.format(DateTime.now()));
 
    //await prefs.setString("timeStamp",)
    //print(response.body);
  }

  void logSavedValue() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String frParam = prefs.getString("fr");
    String toParam = prefs.getString("to");

  }
}