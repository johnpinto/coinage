import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HistoryBar extends StatefulWidget {
  final List<String> history;
  const HistoryBar(this.history);
  @override
  _HistoryBarState createState() => _HistoryBarState();
}

class _HistoryBarState extends State<HistoryBar> {
  SharedPreferences prefs;

  @override
  void initState() {
    super.initState();
    //_loadPreferences();
  }

  //_loadPreferences() async {
  //  prefs = await SharedPreferences.getInstance();
  //  history = prefs.getStringList("history");
  //}

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(25),
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(
              color: Theme.of(context).primaryColor,
              width: 3.0,
              style: BorderStyle.solid),
          borderRadius: BorderRadius.circular(25),
        ),
        height: 180.0,
        width: 400.0,
        child: ListView.builder(
          itemCount: widget.history.length,
          itemBuilder: (context, index) {
            final item = widget.history[index];
            return Dismissible(
              key: Key(item),
              onDismissed: (direction) {
                setState(() {
                  widget.history.removeAt(index);
                });
                _updateHistoryPrefs(widget.history);
              },
              background: Container(
                color: Colors.red,
              ),
              child: ListTile(
                title: Text("$item"),
              ),
            );
          },
        ),
      ),
    );
  }

  _updateHistoryPrefs(history) async {
    prefs = await SharedPreferences.getInstance();
    prefs.setStringList("history", history);
  }
}
