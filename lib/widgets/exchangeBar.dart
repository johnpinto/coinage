import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:Coinage/data/service.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:Coinage/models/currency.dart';

class ExchangeBar extends StatefulWidget {
  final MoneyMaskedTextController inputController;
  final MoneyMaskedTextController outputController;
  final Map<String, Currency> currencies;
  final Function() refresh;

  const ExchangeBar(
      {Key key, this.inputController, this.outputController, this.currencies, this.refresh})
      : super(key: key);

  @override
  _ExchangeBarState createState() => _ExchangeBarState();
}

class _ExchangeBarState extends State<ExchangeBar> {
  SharedPreferences prefs;
  var outputController =
      MoneyMaskedTextController(decimalSeparator: ",", thousandSeparator: ".");

  @override
  void initState() {
    super.initState();
    _loadPrefs();
  }

  _loadPrefs() async {
    prefs = await SharedPreferences.getInstance();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        CurrencyField(
          currencies: widget.currencies,
          controller: widget.inputController,
        ),
        MaterialButton(
          elevation: 8.0,
          shape: CircleBorder(),
          height: 60.0,
          onPressed: () {
            widget.refresh();
          },
          child: IconTheme(
              data: IconThemeData(color: Theme.of(context).accentColor),
              child: Icon(
                Icons.refresh,
                size: 35,
              )),
          color: Theme.of(context).primaryColor,
        ),
        CurrencyField(
            isInput: false,
            currencies: widget.currencies,
            controller: widget.outputController),
      ],
    );
  }

  _swapFields() {
    setState(() {});
  }
}

class CurrencyField extends StatefulWidget {
  final bool isInput;
  final Map<String, Currency> currencies;
  final MoneyMaskedTextController controller;

  const CurrencyField(
      {Key key, this.isInput = true, this.currencies, this.controller})
      : super(key: key);

  @override
  _CurrencyFieldState createState() => _CurrencyFieldState();
}

class _CurrencyFieldState extends State<CurrencyField> {
  SharedPreferences prefs;
  Currency currentCurrency;
  String value;

  @override
  void initState() {
    super.initState();
    this.currentCurrency =
        widget.isInput ? widget.currencies["USD"] : widget.currencies["BRL"];
    print(this.currentCurrency.name);
    _loadPrefs();
  }

  _loadPrefs() async {
    prefs = await SharedPreferences.getInstance();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(maxWidth: 155.0, maxHeight: 60.0),
      child: TextField(
        enableInteractiveSelection: false,
        readOnly: true,
        cursorColor: Theme.of(context).primaryColor,
        decoration: InputDecoration(
          prefix: Text(currentCurrency.codeCool,
          style: TextStyle(color: Colors.black),),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(25.0),
              borderSide: BorderSide(
                  color: Theme.of(context).primaryColor, width: 3.0)),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(25.0),
              borderSide: BorderSide(
                  color: Theme.of(context).primaryColor, width: 3.0)),
        ),
        keyboardType: TextInputType.number,
        inputFormatters: <TextInputFormatter>[
          WhitelistingTextInputFormatter.digitsOnly
        ],
        style: TextStyle(
          fontFamily: "Poppins",
        ),
        textDirection: TextDirection.rtl,
        controller: widget.controller,
        toolbarOptions: ToolbarOptions(
          cut: false,
          copy: false,
          selectAll: false,
          paste: false,
        ),
        onTap: (){
          showDialog(
                context: context,
                builder: (BuildContext context){
                  return SimpleDialog(
                    title: Text("Selecione a moeda desejada"),
                    children: <Widget>[
                      _currencyDialogOption(widget.currencies["BRL"]),
                      _currencyDialogOption(widget.currencies["USD"]),
                      _currencyDialogOption(widget.currencies["EUR"]),
                      _currencyDialogOption(widget.currencies["JPY"]),
                      _currencyDialogOption(widget.currencies["CNY"])
                    ],
                  );
                }
              );
        },
      ),
    );
  }

  Widget _currencyDialogOption(Currency currency) {
    return SimpleDialogOption(
      child: Text("${currency.flag} ${currency.name}",
          style: TextStyle(fontSize: 20.0)),
      onPressed: () {
        setState(() {
          currentCurrency = currency;
          _updateCurrencyPref(currency.code);
          print(currentCurrency.code);
          Service().getConversion();
          Navigator.pop(context);
        });
      },
    );
  }

  void _updateCurrencyPref(String currency) async {
    await prefs.setString(widget.isInput ? "fr" : "to", currency);
  }
}
